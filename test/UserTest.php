<?php

use PHPUnit\Framework\TestCase;
use App\User;

class UserTest extends TestCase{

    protected $User, $userName, $userLastName, $userEmail, $userEdge;

    //validamos que la clase usuario permita registrar nombre, apellido e email.
    public function setUp() : void{
        $this->user = new User;
        $this->userName = "Dany Pascual";
        $this->userLastName = "Gomez Sanchez";
        $this->userEmail = "pascualdas@gmail.com";
        $this->userEdge = 34;
    }

    // Valido que la clase User permita setear el nombre, que lo retorne y que sea string
    public function test_i_can_reg_the_name(){

        $this->user->setName($this->userName);
        $this->assertIsString($this->user->getName());
        $this->assertEquals($this->user->getName(), $this->userName);
    }

    // Valido que la clase User permita setear el apellido, que lo retorne y que sea string
    public function test_i_can_reg_the_lastname()
    {
        $this->user->setLastName($this->userLastName);
        $this->assertIsString($this->user->getLastName());
        $this->assertEquals($this->user->getLastName(), $this->userLastName);
    }

    // Valido que la clase User me retorne el nombre y apellido completos
    public function test_i_can_reg_the_fullName()
    {
        $this->user->setName($this->userName);
        $this->user->setLastName($this->userLastName);

        $this->assertIsString($this->user->getName());
        $this->assertIsString($this->user->getLastName());

        $this->assertEquals($this->user->getLastFullName(), $this->userName.' '.$this->userLastName);
    }


    // Valido que la clase User permita setear el correo, y que lo retorne tenga formato de email
    public function test_i_can_reg_the_email()
    {
        $this->user->setEmail($this->userEmail);
        //Valida formato del email
        $this->assertSame(strtolower($this->user->getEmail()), filter_var(strtolower($this->user->getEmail()), FILTER_VALIDATE_EMAIL));
        $this->assertSame(strtolower($this->user->getEmail()), $this->userEmail);
    }

    // Valido que la clase User permita setear la edad, y que lo retorne sea un entero
    public function test_i_can_reg_the_edge()
    {
        $this->user->setEdge($this->userEdge);
        $this->assertIsInt($this->user->getEdge());
        $this->assertSame($this->user->getEdge(), $this->userEdge);
    }

    // Utilizo la funcion de limpiar valores de propiedades de la clase Users y pruebo que retorne la concatenacion vacia
    public function test_clear_all_properties(){
        $this->user->clearAllProperties();
        $this->assertEmpty($this->user->getName(). $this->user->getLastName() . $this->user->getEmail() . $this->user->getEdge());
    }

    // Validamos que no permita almacenar valores con espacios vacios
    public function test_name_lastName_Email_without_spaces(){
        
        $this->user->setName(' DANY PASCUAL    ');
        $this->user->setLastName('GOMEZ SANCHEZ   ');
        $this->user->setEmail(' PASCUALDAS@GMAIL.COM    ');

        $this->assertEquals($this->user->getName(), 'Dany Pascual');
        $this->assertEquals($this->user->getLastName(), 'Gomez Sanchez');
        $this->assertEquals($this->user->getEmail(), 'pascualdas@gmail.com');

    }

}

?>