<?php

namespace App;

class User{

    protected $userName, $UserLastName, $UserEdge, $userEmail;

    public function setName($userName){
        $this->userName = ucwords(strtolower(trim($userName)));
    }

    public function getName(){
        return $this->userName;
    }

    public function setLastName($UserLastName)
    {
        $this->UserLastName = ucwords(strtolower(trim($UserLastName)));
    }

    public function getLastName()
    {
        return $this->UserLastName;
    }

    public function getLastFullName(){
        return "$this->userName $this->UserLastName";
    }

    public function setEmail($userEmail)
    {
        $this->userEmail = strtolower(trim($userEmail));
    }

    public function getEmail()
    {
        return $this->userEmail;
    }

    public function setEdge($UserEdge)
    {
        $this->UserEdge = $UserEdge;
    }

    public function getEdge()
    {
        return $this->UserEdge;
    }

    public function clearAllProperties(){
        $this->userName = "";
        $this->UserLastName = "";
        $this->userEmail = "";
        $this->UserEdge = "";
    }

}

?>